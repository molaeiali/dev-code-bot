# Dev-Code-Bot

A Telegram Bot to see verification codes during development on Telegram

## Attention - Security

This is just for using in **development** phase, this is not a secure path for two-factor authentication

This Telegram Bot is **not storing any of your codes** as you can see in the code, you can also **unsubscribe** at any time to remove the username you used in your project

## What is this?

If you have two-factor registerations with email or phone verification, You konw that there is always a pain to charge the SMS service to send testing codes to you and your team's phone number.
You can now get those codes in [Telegram](https://telegram.org) with very small effort.

## Usage
### Step 1 - Setting up the Robot

1. Go to [Dev Code Bot](https://t.me/dev_code_bot) on Telegram and create a project using `/create_project` and get a **token**
2. Give the **token** to your team and ask them to join the robot and subscribe to your project using `/subscribe` and the usernames they use to login to your site (Yes, usernames is plural, you can subscribe with more than one username)
3. Enjoy!

### Step 2 - Setting up your Project to send codes to robot
There are two ways you can do this.

#### If you are using Node:
You can use [dev-code](https://www.npmjs.com/package/dev-code)
```
npm install dev-code
```
or
```
yarn add dev-code
```
In your project:
```js
const devcode = require("dev-code");

devcode.send(
    "DEVCODE_TOKEN",
    "USERNAME",
    "CODE",
    "SELF_HOSTED_DOMAIN"
).then((response)=>{
    // Do your work
}).catch((error)=>{
    // Handle network error
});
```
`SELF_HOSTED_DOMAIN` is optional, use it if you self-hosted the telegram bot

By default, `http://dev-code.ir` will be used

#### If you are *not* using Node:

Send a `GET` request to:

<pre style="white-space: pre-wrap;">
http://dev-code.ir/send?token=TOKEN&uname=USERNAME&text=CODE
</pre>

# Donate
Your donation will be used to pay for hosting of the robot
* [PayPal](https://paypal.me/molaeiali)
* [Zarinpal](https://molaei.org/donate)

# Attributions
Icon made by Freepik from www.flaticon.com
