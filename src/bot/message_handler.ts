import { TelegrafContext } from 'telegraf/typings/context'
import { User, State } from '../models/user'
import { create_project_name } from './project'
import { subscribe_token, subscribe_username, unsubscribe_id } from './subscribe'

export const message_handler = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        switch (user.state) {
            case State.CPROJECT_START: {
                await create_project_name(ctx, user)
                break
            }
            case State.SUBSCRIBE_START: {
                await subscribe_token(ctx, user)
                break
            }
            case State.SUBSCRIBE_TOKEN: {
                await subscribe_username(ctx, user)
                break
            }
            case State.UNSUBSCRIBE_START: {
                await unsubscribe_id(ctx, user)
                break
            }
        }
    }
}