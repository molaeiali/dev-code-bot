import { Project } from '../models/project'
import { User, State } from '../models/user'
import { v4 as uuidv4 } from 'uuid'
import { TelegrafContext } from 'telegraf/typings/context'
import * as AsciiTable from 'ascii-table'

export const create_project_start = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        user.state = State.CPROJECT_START
        await user.save().then(() => {
            ctx.reply('Enter a name for your project')
        }).catch((error) => {
            console.error(error)
        })
    }
}

export const create_project_name = async (ctx: TelegrafContext, user: User) => {
    const project = new Project()
    project.name = ctx.message?.text || ''
    project.token = uuidv4()
    project.creator = user
    project.save().then(async () => {
        user.state = State.CLEAR
        await user.save().then(() => {
            ctx.reply(
                `Project '${project.name}' is created, your token is \`${project.token}\`\n\nUse it to subscribe to the project with a username`,
                { parse_mode: 'MarkdownV2' }
            )
        }).catch((error) => {
            console.error(error)
        })
    }).catch((error) => {
        console.error(error)
    })
}

export const get_projects = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id }, { relations: ['owned_projects'] })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        if (user.owned_projects.length > 0) {
            const table = new AsciiTable()
            table.setHeading('name', 'token')
            user.owned_projects.forEach((item) => {
                table.addRow(item.name, item.token)
            })
            ctx.reply('Your projects are:\n\n' +
                '```' + table.toString() + '```',
                { parse_mode: 'MarkdownV2' }
            )
        } else {
            ctx.reply('You have no projects, create one using /create_project')
        }
    }
}