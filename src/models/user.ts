import { Project } from './project'
import { Subscription } from './subscription'
import { Entity, BaseEntity, PrimaryColumn, Column, OneToMany } from 'typeorm'

export enum State {
    CLEAR,
    SUBSCRIBE_START, SUBSCRIBE_TOKEN,
    UNSUBSCRIBE_START,
    CPROJECT_START
}

@Entity()
export class User extends BaseEntity {
    @PrimaryColumn()
    telegram_id: number

    @Column()
    chat_id: number

    @Column('int', { default: State.CLEAR })
    state: State

    @Column({ default: -1 })
    state_helper_id: number

    @OneToMany(type => Project, project => project.creator)
    owned_projects: Project[]

    @OneToMany(type => Subscription, subscription => subscription.user)
    subscriptions: Subscription[]
}