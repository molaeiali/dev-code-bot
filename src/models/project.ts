import { User } from './user'
import { Subscription } from './subscription'
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm'

@Entity()
export class Project extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({ unique: true })
    token: string

    @ManyToOne(type => User, user => user.owned_projects)
    creator: User

    @OneToMany(type => Subscription, subscription => subscription.project)
    subscribers: Subscription[]
}