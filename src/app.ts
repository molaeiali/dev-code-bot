import 'reflect-metadata'
import { start } from './bot/start'
import { Telegraf } from 'telegraf'
import { Subscription } from './models/subscription'
import { message_handler } from './bot/message_handler'
import { create_project_start, get_projects } from './bot/project'
import { createConnection, createQueryBuilder } from 'typeorm'
import { subscribe_start, get_subscribtions, unsubscribe_start } from './bot/subscribe'
import * as express from 'express'
import * as requestify from 'requestify'
import * as Joi from '@hapi/joi'
import * as dotenv from 'dotenv'

const dotenv_result = dotenv.config()
if (dotenv_result.error) {
    throw dotenv_result.error
}

const initDb = async () => {
    await createConnection().then(connection => {
        console.log('Connected successfully to the Database')
    }).catch(error => {
        console.error(error)
    })
}

const initBot = () => {
    const bot = new Telegraf(process.env.BOT_TOKEN || '')

    bot.telegram.setMyCommands(
        [
            { command: 'create_project', description: 'Create a Project' },
            { command: 'subscribe', description: 'Subscribe to a Project' },
            { command: 'unsubscribe', description: 'Unsubscribe from a Project' },
            { command: 'get_projects', description: 'Get list of your projects' },
            { command: 'get_subscribtions', description: 'Get list of your subscribtions' }
        ]
    )

    bot.telegram.setWebhook(process.env.WEBHOOK_URL || '')

    bot.start(start)

    bot.command('subscribe', subscribe_start)
    bot.command('unsubscribe', unsubscribe_start)
    bot.command('create_project', create_project_start)
    bot.command('get_projects', get_projects)
    bot.command('get_subscribtions', get_subscribtions)

    bot.on('message', message_handler)

    bot.launch().then(() => {
        console.log('Bot started!')
    }).catch((error) => {
        console.error(error)
    })
}

const initServer = () => {
    const app = express()

    app.get('/', (req: express.Request, res: express.Response) => {
        res.redirect('https://gitlab.com/molaeiali/dev-code-bot')
    })

    app.get(process.env.WEBHOOK_ROUTE || '', async (req: express.Request, res: express.Response) => {
        const { value, error } = Joi.object({
            token: Joi.string().trim().uuid().required(),
            uname: Joi.string().trim().required(),
            text: Joi.string().trim().required()
        }).validate(req.query)
        if (error) {
            res.status(400).send({ error: error.message })
        } else {
            const rows = await createQueryBuilder('Subscription')
                .leftJoinAndSelect('Subscription.project', 'Project')
                .leftJoinAndSelect('Subscription.user', 'User')
                .where('Project.token = :token AND Subscription.username = :username', { token: value.token, username: value.uname })
                .getMany()

            if (rows.length == 0) {
                res.status(404).send({ error: 'Account you want to send text to does not exsits' })
            } else {
                rows.forEach(async (item) => {
                    await requestify.get('https://api.telegram.org/bot' + process.env.BOT_TOKEN + '/sendMessage?chat_id=' + (item as Subscription).user.chat_id + '&text=' + `Code for project '${(item as Subscription).project.name}':\n${value.text}`)
                })
                res.status(200).send({ msg: 'message sent' })
            }
        }
    })
    app.listen(process.env.PORT, () => {
        console.log('DevCode started on port ' + process.env.PORT)
    })
}

const init = async () => {
    await initDb()
    initBot()
    initServer()
}

init()